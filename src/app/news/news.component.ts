import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { News, INFLUENCE_ARROW_DICT } from '../model/news.model';

@Component({
  selector: 'seg-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  newsList: News[];

  influenceArrow: string;

  constructor(
    private newsService: NewsService
  ) { }

  ngOnInit() {
    this.newsService.getNewsList().subscribe(
      (newsList: News[]) => {
        this.newsList = newsList;
      });
  }

  getInfluenceArrow(news: News): string {
    switch (news.direction) {
      case 'up':
        return INFLUENCE_ARROW_DICT.UP;

      case 'down':
        return INFLUENCE_ARROW_DICT.DOWN;
    }
  }

  flipNews(news: News): void {
    this.newsService.flipNews(news);
  }

  removeNews(news: News): void {
    this.newsService.removeNewsItem(news);
  }

}
