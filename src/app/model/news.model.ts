import { Country } from './country.model';
import { Commodity, COMMODITY_TYPE_DICT, CommodityType } from './exchange.model';

export class News {
  id: number;
  country: Country;
  commodity: Commodity;
  commodityType: CommodityType;
  influenceLevel: string;
  direction: string;
  influence: number;

  constructor(
      country: Country,
      commodity: Commodity,
      commodityType: CommodityType,
      influenceLevel: string,
      direction: string
  ) {
    this.id = Date.now();
    this.country = country;
    this.commodity = commodity;
    this.commodityType = commodityType;
    this.influenceLevel = influenceLevel;
    this.direction = direction;

    switch (this.influenceLevel) {
      case 'low':
        // this.influence = 1 / 6;
        this.influence = 1 / 10;
        break;

      case 'medium':
        this.influence = 3 / 10;
        break;

      case 'high':
        this.influence = 8 / 10;
        break;
    }

    if (this.direction === 'down') {
      this.influence = -this.influence;
    }
  }

  flipDirection(): string {
    switch (this.direction) {
      case 'up':
        this.direction = 'down';
        break;

      case 'down':
        this.direction = 'up';
        break;
    }
    return this.direction;
  }

  isAboutCountry(country: Country): boolean {
    return this.country.name === country.name;
  }

  isAboutCommodity(commodity: Commodity): boolean {
    return (this.commodity !== undefined && this.commodity.name === commodity.name);
  }

  isRelevantToCommodity(commodity: Commodity): boolean {
    if (this.isAboutCommodity(commodity)) {
      return true;
    } else if (this.commodityType === COMMODITY_TYPE_DICT.BOTH
        || this.commodityType === commodity.commodityType) {
      return true;
    }
  }
}

export const INFLUENCE_ARROW_DICT = {
  UP: '&#x25b4;',
  DOWN: '&#x25be;'
};
