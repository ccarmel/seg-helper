export class Country {
  name: string;
  commodityInvestmentDict: { [commodityName: string]: number; };
  investmentProportionPerCommodity: number;
  totalInvestmentProportion: number;

  constructor(name: string) {
    this.name = name;
    this.commodityInvestmentDict = {};
    this.investmentProportionPerCommodity = 0;
    this.totalInvestmentProportion = 0;
  }
}

export const COUNTRY_LIST: Country[] = [
  new Country('argentina'),
  new Country('australia'),
  new Country('brazil'),
  new Country('canada'),
  new Country('china'),
  new Country('colombia'),
  new Country('france'),
  new Country('germany'),
  new Country('india'),
  new Country('indonesia'),
  new Country('iran'),
  new Country('ivory coast'),
  new Country('malaysia'),
  new Country('mexico'),
  new Country('nigeria'),
  new Country('pakistan'),
  new Country('peru'),
  new Country('qatar'),
  new Country('russia'),
  new Country('saudi arabia'),
  new Country('south africa'),
  new Country('spain'),
  new Country('thailand'),
  new Country('usa'),
  new Country('uzbekistan'),
  new Country('vietnam'),
];
