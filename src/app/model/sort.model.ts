import { COUNTRY_LIST, Country } from './country.model';
import { EXCHANGE_LIST, Exchange, Commodity } from './exchange.model';

export const NUMBER_FORMAT = '1.5-5';

export const SORT_ARROW_DICT = {
  LEFT: '&#x2190;',
  UP: '&#x2191;',
  RIGHT: '&#x2192;',
  DOWN: '&#x2193;',

  LEFT_RIGHT: '&#x2194;',
  UP_DOWN: '&#x2195;'
};

export const SortArrowLookUp = {
  horizontal: {
    forward: SORT_ARROW_DICT.RIGHT,
    backward: SORT_ARROW_DICT.LEFT,
    neutral: SORT_ARROW_DICT.LEFT_RIGHT
  },
  vertical: {
    forward: SORT_ARROW_DICT.DOWN,
    backward: SORT_ARROW_DICT.UP,
    neutral: SORT_ARROW_DICT.UP_DOWN
  }
};

export class SortArrowReset {
  arrowDirection: string;
  skipSortByValue: string;
}

export class InvestmentTableCell {
  columnName: string;
  rowName: string;

  exchangeName: string;

  isCountryColumn: boolean;
  isExchangeRow: boolean;
  isCommodityRow: boolean;

  constructor(columnName, rowName) {
    this.columnName = columnName;
    this.rowName = rowName;

    this.exchangeName = '';

    this.isCountryColumn = false;
    if (columnName === 'country') {
      this.isCountryColumn = true;
    }

    COUNTRY_LIST.forEach(
      (country: Country) => {
        if (country.name === columnName) {
          this.isCountryColumn = true;
        }
      });

    this.isExchangeRow = false;
    if (columnName === 'exchange') {
      this.isExchangeRow = true;
    }

    this.isCommodityRow = false;
    if (rowName === 'commodity') {
      this.isCommodityRow = true;
    }

    EXCHANGE_LIST.forEach(
      (exchange: Exchange) => {
        if (exchange.name === columnName) {
          this.exchangeName = exchange.name;
        } else {
          exchange.commodityList.forEach(
            (commodity: Commodity) => {
              if (commodity.name === columnName) {
                this.exchangeName = exchange.name;
              }
              if (commodity.name === rowName) {
                this.isCommodityRow = true;
              }
            });
        }
      });
  }
}
