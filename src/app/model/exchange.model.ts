export class Commodity {
  name: string;
  investmentByCountryName: { [countryName: string]: number };
  totalInvestment: number;
  commodityType: CommodityType;
  exchangeName: string;
}

export class CommodityType {
  name: string;
  display: string;
}

export const COMMODITY_TYPE_DICT: { [ type: string ]: CommodityType } = {
  AGRICULTURE: {
    name: 'agriculture',
    display: 'A'
  },
  BOTH: {
    name: 'both',
    display: '+'
  },
  INDUSTRY: {
    name: 'industry',
    display: 'I'
  }
};

export class Exchange {
  name: string;
  feePerUnitByUnitName: {
    opening: number;
    day: number;
  };
  commodityList: Commodity[];
}

export const EXCHANGE_LIST: Exchange[] = [
  {
    name: 'dubai',
    feePerUnitByUnitName: {
      opening: 5 / 100,
      day: .5 / 100
    },
    commodityList: [
      {
        name: 'aluminum',
        investmentByCountryName: {},
        totalInvestment: 0 / 16,
        commodityType: COMMODITY_TYPE_DICT.INDUSTRY,
        exchangeName: 'dubai'
      }, {
        name: 'lead',
        investmentByCountryName: {},
        totalInvestment: 0 / 16,
        commodityType: COMMODITY_TYPE_DICT.INDUSTRY,
        exchangeName: 'dubai'
      }, {
        name: 'coffee',
        investmentByCountryName: {
          brazil: 6 / 16,
          vietnam: 3 / 16,
          colombia: 2 / 16,
          indonesia: 2 / 16,
          peru: 1 / 16
        },
        totalInvestment: 14 / 16,
        commodityType: COMMODITY_TYPE_DICT.AGRICULTURE,
        exchangeName: 'dubai'
      }, {
        name: 'orange juice',
        investmentByCountryName: {
          brazil: 9 / 16,
          usa: 4 / 16,
          mexico: 1 / 16,
          'south africa': .5 / 16,
          spain: .5 / 16
        },
        totalInvestment: 15 / 16,
        commodityType: COMMODITY_TYPE_DICT.AGRICULTURE,
        exchangeName: 'dubai'
      }
    ]
  },
  {
    name: 'tokyo',
    feePerUnitByUnitName: {
      opening: 2.5 / 100,
      day: .25 / 100
    },
    commodityList: [
      {
        name: 'natural gas',
        investmentByCountryName: {
          usa: 4 / 16,
          russia: 4 / 16,
          qatar: 2 / 16,
          iran: 1 / 16,
          canada: 1 / 16
        },
        totalInvestment: 12 / 16,
        commodityType: COMMODITY_TYPE_DICT.INDUSTRY,
        exchangeName: 'tokyo'
      }, {
        name: 'zinc',
        investmentByCountryName: {
          china: 6 / 16,
          peru: 3 / 16,
          australia: 3 / 16,
          india: 2 / 16,
          usa: 2 / 16
        },
        totalInvestment: 16 / 16,
        commodityType: COMMODITY_TYPE_DICT.INDUSTRY,
        exchangeName: 'tokyo'
      }, {
        name: 'cotton',
        investmentByCountryName: {
          china: 4 / 16,
          usa: 4 / 16,
          india: 2 / 16,
          pakistan: 2 / 16,
          uzbekistan: 1 / 16
        },
        totalInvestment: 13 / 16,
        commodityType: COMMODITY_TYPE_DICT.AGRICULTURE,
        exchangeName: 'tokyo'
      }, {
        name: 'palm oil',
        investmentByCountryName: {
          indonesia: 7 / 16,
          malaysia: 6 / 16,
          nigeria: 1 / 16,
          thailand: .5 / 16,
          colombia: .5 / 16
        },
        totalInvestment: 15 / 16,
        commodityType: COMMODITY_TYPE_DICT.AGRICULTURE,
        exchangeName: 'tokyo'
      }
    ]
  },
  {
    name: 'hong kong',
    feePerUnitByUnitName: {
      opening: 1 / 100,
      day: .1 / 100
    },
    commodityList: [
      {
        name: 'oil',
        investmentByCountryName: {
          russia: 3 / 16,
          'saudi arabia': 3 / 16,
          usa: 2 / 16,
          china: 1 / 16,
          canada: 1 / 16
        },
        totalInvestment: 10 / 16,
        commodityType: COMMODITY_TYPE_DICT.INDUSTRY,
        exchangeName: 'hong kong'
      }, {
        name: 'milk',
        investmentByCountryName: {
          india: 3 / 16,
          usa: 2 / 16,
          china: 1 / 16,
          pakistan: 1 / 16,
          russia: 1 / 16
        },
        totalInvestment: 8 / 16,
        commodityType: COMMODITY_TYPE_DICT.AGRICULTURE,
        exchangeName: 'hong kong'
      }, {
        name: 'soybeans',
        investmentByCountryName: {
          brazil: 6 / 16,
          usa: 5 / 16,
          argentina: 4 / 16,
          china: 2 / 16,
          india: 1 / 16
        },
        totalInvestment: 18 / 16,
        commodityType: COMMODITY_TYPE_DICT.AGRICULTURE,
        exchangeName: 'hong kong'
      }, {
        name: 'sugar',
        investmentByCountryName: {
          brazil: 5 / 16,
          india: 4 / 16,
          thailand: 3 / 16,
          'ivory coast': 3 / 16,
          russia: 2 / 16
        },
        totalInvestment: 17 / 16,
        commodityType: COMMODITY_TYPE_DICT.AGRICULTURE,
        exchangeName: 'hong kong'
      }
    ]
  },
  {
    name: 'new york',
    feePerUnitByUnitName: {
      opening: .5 / 100,
      day: .05 / 100
    },
    commodityList: [
      {
        name: 'rapeseed',
        investmentByCountryName: {
          china: 4 / 16,
          canada: 4 / 16,
          india: 2 / 16,
          france: 2 / 16,
          germany: 2 / 16
        },
        totalInvestment: 14 / 16,
        commodityType: COMMODITY_TYPE_DICT.AGRICULTURE,
        exchangeName: 'new york'
      }, {
        name: 'wheat',
        investmentByCountryName: {
          china: 4 / 16,
          india: 3 / 16,
          usa: 2 / 16,
          russia: 1 / 16,
          france: 1 / 16
        },
        totalInvestment: 11 / 16,
        commodityType: COMMODITY_TYPE_DICT.AGRICULTURE,
        exchangeName: 'new york'
      }, {
        name: 'coal',
        investmentByCountryName: {
          china: 8 / 16,
          usa: 3 / 16,
          india: 2 / 16,
          australia: 2 / 16,
          russia: 1 / 16
        },
        totalInvestment: 16 / 16,
        commodityType: COMMODITY_TYPE_DICT.INDUSTRY,
        exchangeName: 'new york'
      }, {
        name: 'corn',
        investmentByCountryName: {
          usa: 7 / 16,
          china: 4 / 16,
          brazil: 2 / 16,
          mexico: 1 / 16,
          india: 1 / 16
        },
        totalInvestment: 15 / 16,
        commodityType: COMMODITY_TYPE_DICT.AGRICULTURE,
        exchangeName: 'new york'
      }
    ]
  }
];
