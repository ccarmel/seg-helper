import { Component, OnInit } from '@angular/core';
import { InvestmentService } from '../investment.service';
import { Exchange, Commodity } from '../model/exchange.model';
import { StyleService } from '../style.service';
import { InvestmentTableCell } from '../model/sort.model';
import { NewsService } from '../news.service';

@Component({
  selector: '[seg-exchange-row]',
  templateUrl: './exchange-row.component.html',
  styleUrls: ['./exchange-row.component.css']
})
export class ExchangeRowComponent implements OnInit {

  exchangeList: Exchange[];
  sortByValue: string;

  mouseOverInvestmentTableCell: InvestmentTableCell;

  mouseOverExchangeCommodity: string;

  constructor(
    private investmentService: InvestmentService,
    private newsService: NewsService,
    private styleService: StyleService
  ) {
    this.exchangeList = [];
    this.sortByValue = '';

    this.mouseOverInvestmentTableCell = new InvestmentTableCell('', '');

    this.mouseOverExchangeCommodity = '';
  }

  ngOnInit() {
    this.investmentService.getExchangeList().subscribe(
      (exchangeList: Exchange[]) => {
        this.exchangeList = exchangeList;
      });

    this.investmentService.getCommodityListSortByValue().subscribe(
      (sortByValue: string) => {
        this.sortByValue = sortByValue;
      });

    this.styleService.getMouseOverInvestmentTableCell().subscribe(
      (investmentTableCell: InvestmentTableCell) => {
        this.mouseOverInvestmentTableCell = investmentTableCell;
        this.exchangeList.forEach(
          (exchange: Exchange) => {
            exchange.commodityList.forEach(
              (commodity: Commodity) => {
                if (commodity.name === investmentTableCell.columnName) {
                  this.mouseOverExchangeCommodity = exchange.name;
                }
              });
          });
      });
  }

  sortCommodityListByExchange(sortOrder: string): void {
    this.investmentService.sortCommodityListByExchange(sortOrder);
  }

  onMouseEnter(columnName: string, rowName: string): void {
    this.styleService.setMouseOverInvestmentTableCell(columnName, 'exchange');
  }

  onMouseLeave(): void {
    this.styleService.setMouseOverInvestmentTableCell('', '');
  }

  clearNewsList(): void {
    this.newsService.clearNewsList();
  }
}
