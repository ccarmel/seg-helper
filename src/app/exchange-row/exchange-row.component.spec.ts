import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExchangeRowComponent } from './exchange-row.component';

describe('ExchangeRowComponent', () => {
  let component: ExchangeRowComponent;
  let fixture: ComponentFixture<ExchangeRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExchangeRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
