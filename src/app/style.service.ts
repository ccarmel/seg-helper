import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { InvestmentTableCell } from './model/sort.model';
import { InvestmentService } from './investment.service';

@Injectable({
  providedIn: 'root'
})
export class StyleService {

  mouseOverInvestmentTableCell: InvestmentTableCell;
  mouseOverInvestmentTableCellBehaviorSubject: BehaviorSubject<InvestmentTableCell>;

  constructor() {
    this.mouseOverInvestmentTableCell = new InvestmentTableCell('', '');
    this.mouseOverInvestmentTableCellBehaviorSubject = new BehaviorSubject(this.mouseOverInvestmentTableCell);
  }

  getMouseOverInvestmentTableCell(): Observable<InvestmentTableCell> {
    return this.mouseOverInvestmentTableCellBehaviorSubject.asObservable();
  }

  setMouseOverInvestmentTableCell(columnName: string, rowName: string): void {
    this.mouseOverInvestmentTableCell = new InvestmentTableCell(columnName, rowName);
    this.mouseOverInvestmentTableCellBehaviorSubject.next(this.mouseOverInvestmentTableCell);
  }

  getInfluenceLevelBackgroundColor(influence: number): string {
    if (influence === 0) {
      return '#FFFFFF';
    }

    let redLevel = 255;
    let greenLevel = 255;

    if (influence < 0) {
      greenLevel -= Math.round(255 * -influence);
    } else if (influence > 0) {
      redLevel -= Math.round(255 * influence);
    }

    const backgroundColor = '#' + redLevel.toString(16) + greenLevel.toString(16) + '00';
    return backgroundColor.toUpperCase();
  }
}
