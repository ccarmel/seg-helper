import { Injectable } from '@angular/core';
import { Country, COUNTRY_LIST } from './model/country.model';
import { Commodity, EXCHANGE_LIST, Exchange } from './model/exchange.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { SortArrowReset } from './model/sort.model';
import { NewsService } from './news.service';
import { News } from './model/news.model';

@Injectable({
  providedIn: 'root'
})
export class InvestmentService {
  exchangeList: Exchange[];
  countryList: Country[];
  commodityList: Commodity[];

  countryListSortByValue: string;
  commodityListSortByValue: string;

  newsList: News[];

  exchangeListBehaviorSubject: BehaviorSubject<Exchange[]>;
  countryListBehaviorSubject: BehaviorSubject<Country[]>;
  commodityListBehaviorSubject: BehaviorSubject<Commodity[]>;

  countryListSortByValueBehaviorSubject: BehaviorSubject<string>;
  commodityListSortByValueBehaviorSubject: BehaviorSubject<string>;

  sortArrowResetSubject: BehaviorSubject<SortArrowReset>;

  constructor(
    private newsService: NewsService
  ) {
    this.populateExchangeList();
    this.populateCommodityList();
    this.populateCountryList();

    this.countryListSortByValue = '';
    this.commodityListSortByValue = '';

    this.newsService.getNewsList().subscribe(
      (newsList: News[]) => {
        this.onNewsListUpdate(newsList);
      });

    this.sortArrowResetSubject = new BehaviorSubject({
      arrowDirection: null,
      skipSortByValue: null
    });
  }

  populateExchangeList(): void {
    this.exchangeList = EXCHANGE_LIST;
    this.exchangeListBehaviorSubject = new BehaviorSubject(this.exchangeList);
  }

  populateCommodityList(): void {
    this.commodityList = [];
    EXCHANGE_LIST.forEach(
      (exchange: Exchange) => {
        this.commodityList.push(...exchange.commodityList);
      });
    this.commodityListBehaviorSubject = new BehaviorSubject(this.commodityList);
    this.commodityListSortByValueBehaviorSubject = new BehaviorSubject('exchange');
  }

  populateCountryList(): void {
    this.countryList = COUNTRY_LIST;
    EXCHANGE_LIST.forEach(
      (exchange: Exchange) => {
        exchange.commodityList.forEach(
          (commodity: Commodity) => {
            for (const countryName of Object.keys(commodity.investmentByCountryName)) {
              const investmentValue = commodity.investmentByCountryName[countryName];
              const country = this.countryList.find(
                (currentCountry: Country) => {
                  return currentCountry.name === countryName;
                });
              country.commodityInvestmentDict[commodity.name] = investmentValue / commodity.totalInvestment;
            }
          });
      }
    );

    this.countryList.forEach(
      (country: Country) => {
        for (const commodityName of Object.keys(country.commodityInvestmentDict)) {
          const investmentValue = country.commodityInvestmentDict[commodityName];
          country.totalInvestmentProportion += investmentValue;
        }
        country.investmentProportionPerCommodity =
          country.totalInvestmentProportion / Object.keys(country.commodityInvestmentDict).length;
      });

    this.countryListBehaviorSubject = new BehaviorSubject(this.countryList);
    this.countryListSortByValueBehaviorSubject = new BehaviorSubject('country');
  }

  onNewsListUpdate(newsList: News[]): void {
    if (this.commodityListSortByValue === 'influence') {
      this.sortCommodityListByInfluence('forward');
    }
  }

  private setCommodityListSortByValue(sortByValue: string): void {
    this.commodityListSortByValue = sortByValue;
    this.commodityListSortByValueBehaviorSubject.next(this.commodityListSortByValue);
  }

  private setCountryListSortByValue(sortByValue: string): void {
    this.countryListSortByValue = sortByValue;
    this.countryListSortByValueBehaviorSubject.next(this.countryListSortByValue);
  }

  getExchangeList(): Observable<Exchange[]> {
    return this.exchangeListBehaviorSubject.asObservable();
  }

  getCountryList(): Observable<Country[]> {
    return this.countryListBehaviorSubject.asObservable();
  }

  getCommodityList(): Observable<Commodity[]> {
    return this.commodityListBehaviorSubject.asObservable();
  }

  getCommodityListSortByValue(): Observable<string> {
    return this.commodityListSortByValueBehaviorSubject.asObservable();
  }

  getCountryListSortByValue(): Observable<string> {
    return this.countryListSortByValueBehaviorSubject.asObservable();
  }

  getSortArrowReset(): Observable<SortArrowReset> {
    return this.sortArrowResetSubject.asObservable();
  }

  sortCommodityListByExchange(sortOrder: string) {
    this.exchangeList.sort(
      (thisExchange: Exchange, thatExchange: Exchange) => {
        return thatExchange.feePerUnitByUnitName.opening - thisExchange.feePerUnitByUnitName.opening;
      });

    if (sortOrder === 'backward') {
      this.exchangeList.reverse();
    }

    const sortedCommodityList = [];
    this.exchangeList.forEach(
      (exchange: Exchange) => {
        const sortedExchangeCommodityList = Array.from(exchange.commodityList);
        if (sortOrder === 'backward') {
          sortedExchangeCommodityList.reverse();
        }
        sortedCommodityList.push(...sortedExchangeCommodityList);
      });
    this.commodityList = sortedCommodityList;


    this.sortArrowResetSubject.next({ arrowDirection: 'horizontal', skipSortByValue: 'exchange'});
    this.exchangeListBehaviorSubject.next(this.exchangeList);
    this.commodityListBehaviorSubject.next(this.commodityList);
    this.setCommodityListSortByValue('exchange');
  }

  sortCommodityListByCommodityName(sortOrder: string): void {
    this.commodityList.sort(
      (thisCommodity: Commodity, thatCommodity: Commodity) => {
        return thisCommodity.name.localeCompare(thatCommodity.name);
      });

    if (sortOrder === 'backward') {
      this.commodityList.reverse();
    }

    this.sortArrowResetSubject.next({ arrowDirection: 'horizontal', skipSortByValue: 'commodity'});
    this.commodityListBehaviorSubject.next(this.commodityList);
    this.setCommodityListSortByValue('commodity');
  }

  sortCommodityListByInfluence(sortOrder: string): void {
    this.commodityList.sort(
      (thisCommodity: Commodity, thatCommodity: Commodity) => {
        const thisInfluence = this.newsService.getInfluence(undefined, thisCommodity);
        const thatInfluence = this.newsService.getInfluence(undefined, thatCommodity);
        return thatInfluence - thisInfluence;
      });

    if (sortOrder === 'backward') {
      this.commodityList.reverse();
    }

    this.commodityListBehaviorSubject.next(this.commodityList);
    this.setCommodityListSortByValue('influence');
  }

  sortCommodityListByCountryInvestment(country: Country, sortOrder: string): void {
    const relevantCommodityList = this.commodityList.filter(
      (commodity: Commodity) => {
        return country.commodityInvestmentDict[commodity.name] !== undefined;
      });

    relevantCommodityList.sort(
      (thisCommodity: Commodity, thatCommodity: Commodity) => {
        let thisCommodityInvestiment = country.commodityInvestmentDict[thisCommodity.name];
        if (thisCommodityInvestiment === undefined) {
          thisCommodityInvestiment = 0.0;
        }
        let thatCommodityInvestiment = country.commodityInvestmentDict[thatCommodity.name];
        if (thatCommodityInvestiment === undefined) {
          thatCommodityInvestiment = 0.0;
        }

        return thatCommodityInvestiment - thisCommodityInvestiment;
      });

    if (sortOrder === 'backward') {
      relevantCommodityList.reverse();
    }

    const irrelevantCommodityList = this.commodityList.filter(
      (commodity: Commodity) => {
        return country.commodityInvestmentDict[commodity.name] === undefined;
      });

    this.commodityList = relevantCommodityList.concat(irrelevantCommodityList);

    this.sortArrowResetSubject.next({ arrowDirection: 'horizontal', skipSortByValue: country.name});
    this.commodityListBehaviorSubject.next(this.commodityList);
    this.setCommodityListSortByValue(country.name);
  }

  sortCountryListByCountryName(sortOrder: string): void {
    this.countryList.sort(
      (thisCountry: Country, thatCountry: Country) => {
        return thisCountry.name.localeCompare(thatCountry.name);
      });

    if (sortOrder === 'backward') {
      this.countryList.reverse();
    }

    this.sortArrowResetSubject.next({ arrowDirection: 'vertical', skipSortByValue: 'country'});
    this.countryListBehaviorSubject.next(this.countryList);
    this.setCountryListSortByValue('country');
  }

  sortCountryListByCommodityInvestment(commodity: Commodity, sortOrder: string): void {
    const relevantCountryList = this.countryList.filter(
      (country: Country) => {
        return country.commodityInvestmentDict[commodity.name] !== undefined;
      });

    relevantCountryList.sort(
      (thisCountry: Country, thatCountry: Country) => {
          let thisCountryInvestiment = thisCountry.commodityInvestmentDict[commodity.name];
          if (thisCountryInvestiment === undefined) {
            thisCountryInvestiment = 0.0;
          }
          let thatCountryInvestiment = thatCountry.commodityInvestmentDict[commodity.name];
          if (thatCountryInvestiment === undefined) {
            thatCountryInvestiment = 0.0;
          }

          return thatCountryInvestiment - thisCountryInvestiment;
        });

    if (sortOrder === 'backward') {
      relevantCountryList.reverse();
    }

    const irrelevantCountryList = this.countryList.filter(
    (country: Country) => {
      return country.commodityInvestmentDict[commodity.name] === undefined;
    });

    this.countryList = relevantCountryList.concat(irrelevantCountryList);

    this.sortArrowResetSubject.next({ arrowDirection: 'vertical', skipSortByValue: commodity.name});
    this.countryListBehaviorSubject.next(this.countryList);
    this.setCountryListSortByValue(commodity.name);
  }

  sortCountryListByCountryStatistic(statistic: string, sortOrder: string) {
    this.countryList.sort(
      (thisCountry: Country, thatCountry: Country) => {
        return thatCountry[statistic] - thisCountry[statistic];
      });

    if (sortOrder === 'backward') {
      this.countryList.reverse();
    }

    this.sortArrowResetSubject.next({ arrowDirection: 'vertical', skipSortByValue: statistic});
    this.countryListBehaviorSubject.next(this.countryList);
    this.setCountryListSortByValue(statistic);
  }
}
