import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Country } from '../model/country.model';
import { InvestmentService } from '../investment.service';
import { Commodity } from '../model/exchange.model';
import { SortByComponent } from '../sort-by/sort-by.component';
import { StyleService } from '../style.service';
import { NUMBER_FORMAT, InvestmentTableCell } from '../model/sort.model';
import { ThrowStmt } from '@angular/compiler';
import { NewsService } from '../news.service';

@Component({
  selector: '[seg-country-row]',
  templateUrl: './country-row.component.html',
  styleUrls: ['./country-row.component.css']
})
export class CountryRowComponent implements OnInit {

  @Input() country: Country;

  @ViewChild('sortByCountryInvestment', {static: false})
  countryInvestmentSortByComponent: SortByComponent;

  numberFormat: string;
  commodityList: Commodity[];

  showNewsControlsDict: { [columnName: string]: boolean };

  mouseOverInvestmentTableCell: InvestmentTableCell;

  constructor(
    private investmentService: InvestmentService,
    private newsService: NewsService,
    private styleService: StyleService
  ) {
    this.numberFormat = NUMBER_FORMAT;
    this.commodityList = [];
    this.showNewsControlsDict = {};

    this.mouseOverInvestmentTableCell = new InvestmentTableCell('', '');
  }

  ngOnInit() {
    this.showNewsControlsDict[this.country.name] = false;
    this.investmentService.getCommodityList().subscribe(
      (commodityList: Commodity[]) => {
        this.commodityList = commodityList;
        this.commodityList.forEach(
          (commodity: Commodity) => {
            this.showNewsControlsDict[commodity.name] = false;
          });
      });

    this.styleService.getMouseOverInvestmentTableCell().subscribe(
      (investmentTableCell: InvestmentTableCell) => {
        this.mouseOverInvestmentTableCell = investmentTableCell;
      });
  }

  sortCommodityListByCountryInvestment(sortOrder: string): void {
    this.investmentService.sortCommodityListByCountryInvestment(this.country, sortOrder);
  }

  toggleNewsControls(columnName: string): void {
    this.showNewsControlsDict[columnName] = !this.showNewsControlsDict[columnName];
  }

  getInfluenceLevelBackgroundColor(country: Country, commodity: Commodity): string {
    return this.newsService.getInfluenceLevelBackgroundColor(country, commodity);
  }

  onMouseEnter(columnName: string): void {
    this.styleService.setMouseOverInvestmentTableCell(columnName, this.country.name);
  }

  onMouseLeave(): void {
    this.styleService.setMouseOverInvestmentTableCell('', '');
  }
}
