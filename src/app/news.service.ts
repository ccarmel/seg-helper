import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { News } from './model/news.model';
import { Commodity } from './model/exchange.model';
import { Country } from './model/country.model';
import { StyleService } from './style.service';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  newsList: News[];
  newsListBehaviorSubject: BehaviorSubject<News[]>;

  constructor(
    private styleService: StyleService
  ) {
    this.newsList = [];
    this.newsListBehaviorSubject = new BehaviorSubject(this.newsList);
  }

  getNewsList(): Observable<News[]> {
    return this.newsListBehaviorSubject.asObservable();
  }

  flipNews(news: News): void {
    const newsIndex = this.newsList.indexOf(news);
    const flippedDirection = news.flipDirection();
    const updatedNews = new News(news.country, news.commodity, news.commodityType, news.influenceLevel, flippedDirection);
    this.newsList.splice(newsIndex, 1, updatedNews);
    this.newsListBehaviorSubject.next(this.newsList);
  }

  addNewsItem(news: News) {
    this.newsList.push(news);
    this.newsListBehaviorSubject.next(this.newsList);
  }

  removeNewsItem(news: News) {
    this.newsList = this.newsList.filter((currentNews: News) => currentNews.id !== news.id);
    this.newsListBehaviorSubject.next(this.newsList);
  }

  clearNewsList(): void {
    this.newsList = [];
    this.newsListBehaviorSubject.next(this.newsList);
  }

  getInfluenceLevelBackgroundColor(country: Country, commodity: Commodity): string {
    const influence = this.getInfluence(country, commodity);
    return this.styleService.getInfluenceLevelBackgroundColor(influence);
  }

  getInfluence(country: Country, commodity: Commodity): number {
    let totalInfluence = 0.0;
    this.newsList.forEach(
      (news: News) => {
        if (country !== undefined) {
          if (commodity === undefined) {

            // if country but no commodity
            if (news.isAboutCountry(country) && news.commodity === undefined) {
              totalInfluence += news.influence;
            }

          } else {

            // if country and commodity
            if (news.isAboutCountry(country) && news.isRelevantToCommodity(commodity)) {
              totalInfluence += news.influence;
            }

          }
        } else {
          if (commodity !== undefined) {

            // if commodity but no country
            if (news.isRelevantToCommodity(commodity)) {
              const countryCommodityInvesment = news.country.commodityInvestmentDict[commodity.name];
              if (countryCommodityInvesment !== undefined) {
                totalInfluence += (news.influence * countryCommodityInvesment);
              }
            }

          }
        }
      });

    return totalInfluence;
  }
}
