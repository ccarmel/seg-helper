import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { InvestmentTableComponent } from './investment-table/investment-table.component';
import { CountryRowComponent } from './country-row/country-row.component';
import { CommodityRowComponent } from './commodity-row/commodity-row.component';
import { HtmlEntityPipe } from './html-entity.pipe';
import { SortByComponent } from './sort-by/sort-by.component';
import { ExchangeRowComponent } from './exchange-row/exchange-row.component';
import { NewsInputComponent } from './news-input/news-input.component';
import { NewsComponent } from './news/news.component';

@NgModule({
  declarations: [
    AppComponent,
    InvestmentTableComponent,
    CountryRowComponent,
    CommodityRowComponent,
    HtmlEntityPipe,
    SortByComponent,
    ExchangeRowComponent,
    NewsInputComponent,
    NewsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
