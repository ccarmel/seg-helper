import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommodityRowComponent } from './commodity-row.component';

describe('CommodityRowComponent', () => {
  let component: CommodityRowComponent;
  let fixture: ComponentFixture<CommodityRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommodityRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommodityRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
