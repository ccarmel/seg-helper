import { Component, OnInit, ViewChild, ViewChildren, QueryList, Output, EventEmitter } from '@angular/core';
import { Commodity } from '../model/exchange.model';
import { InvestmentService } from '../investment.service';
import { SortByComponent } from '../sort-by/sort-by.component';
import { StyleService } from '../style.service';
import { COUNTRY_LIST, Country } from '../model/country.model';
import { InvestmentTableCell } from '../model/sort.model';
import { NewsService } from '../news.service';

@Component({
  selector: '[seg-commodity-row]',
  templateUrl: './commodity-row.component.html',
  styleUrls: ['./commodity-row.component.css']
})
export class CommodityRowComponent implements OnInit {

  @ViewChildren(SortByComponent)
  sortByComponentQueryList: QueryList<SortByComponent>;

  commodityList: Commodity[];

  mouseOverInvestmentTableCell: InvestmentTableCell;

  countryNameList: string[];
  mouseOverCountryColumn: boolean;

  constructor(
    private investmentService: InvestmentService,
    private newsService: NewsService,
    private styleService: StyleService
  ) {
    this.commodityList = [];

    this.mouseOverInvestmentTableCell = new InvestmentTableCell('', '');

    this.mouseOverCountryColumn = false;

    this.countryNameList = [];
    COUNTRY_LIST.forEach(
      (country: Country) => {
        this.countryNameList.push(country.name);
      });
  }

  ngOnInit() {
    this.investmentService.getCommodityList().subscribe(
      (commodityList: Commodity[]) => {
        this.commodityList = commodityList;
      });

    this.styleService.getMouseOverInvestmentTableCell().subscribe(
      (mouseOverInvestmentTableCell: InvestmentTableCell) => {
        this.mouseOverInvestmentTableCell = mouseOverInvestmentTableCell;
      });
  }

  getInfluenceLevelBackgroundColor(country: Country, commodity: Commodity): string {
    return this.newsService.getInfluenceLevelBackgroundColor(country, commodity);
  }

  sortCountryListByCountryName(sortOrder: string): void {
    this.investmentService.sortCountryListByCountryName(sortOrder);
  }

  sortCommodityListByCommodityName(sortOrder: string): void {
    this.investmentService.sortCommodityListByCommodityName(sortOrder);
  }

  sortCommodityListByInfluence(sortOrder: string): void {
    this.investmentService.sortCommodityListByInfluence(sortOrder);
  }

  sortCountryListByCommodityInvestment(commodity: Commodity, sortOrder: string): void {
    this.investmentService.sortCountryListByCommodityInvestment(commodity, sortOrder);
  }

  sortCountryListByCountryStatistic(statistic: string, sortOrder: string): void {
    this.investmentService.sortCountryListByCountryStatistic(statistic, sortOrder);
  }

  onMouseEnter(columnName: string, rowName: string): void {
    this.styleService.setMouseOverInvestmentTableCell(columnName, rowName);
  }

  onMouseLeave(): void {
    this.styleService.setMouseOverInvestmentTableCell('', '');
  }
}
