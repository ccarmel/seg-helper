import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { NewsService } from '../news.service';
import { News, INFLUENCE_ARROW_DICT } from '../model/news.model';
import { Country } from '../model/country.model';
import { Commodity, COMMODITY_TYPE_DICT, CommodityType } from '../model/exchange.model';

@Component({
  selector: 'seg-news-input',
  templateUrl: './news-input.component.html',
  styleUrls: ['./news-input.component.css']
})
export class NewsInputComponent implements OnInit {

  @Input() country: Country;
  @Input() commodity: Commodity;

  @Input() showInput: boolean;
  @Output() showInputChange: EventEmitter<boolean>;

  influenceLevelList: string[];
  influenceArrowDict: { UP: string; DOWN: string; };

  commodityTypeDict: { [type: string]: CommodityType };
  selectedCommodityType: any;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private newsService: NewsService
  ) {
    this.showInput = false;
    this.showInputChange = new EventEmitter<boolean>();

    this.influenceLevelList = ['low', 'medium', 'high'];
    this.influenceArrowDict = INFLUENCE_ARROW_DICT;

    this.commodityTypeDict = COMMODITY_TYPE_DICT;
    this.selectedCommodityType = COMMODITY_TYPE_DICT.BOTH;
  }

  ngOnInit() {
    this.newsService.getNewsList().subscribe(
      (newsList: News[]) => {
        if (this.commodity !== undefined) {
        if (this.country.name === 'india' && this.commodity.name === 'cotton') {
          console.log('aaa');
        }
      }
        this.showInput = false;
        this.changeDetectorRef.detectChanges();
      });
  }

  swallowClick(event: Event): void {
    event.stopPropagation();
  }

  toggleShowInput(showInput: boolean): void {
    if (showInput === undefined) {
      this.showInput = !this.showInput;
    } else {
      this.showInput = showInput;
    }
    this.showInputChange.emit(this.showInput);
  }

  createNewsItem(influenceLevel: string, direction: string): void {
    let tempSelectedCommodityType;
    if (this.commodity === undefined && this.selectedCommodityType !== undefined) {
      tempSelectedCommodityType = this.selectedCommodityType;
    }

    const news = new News(this.country, this.commodity, tempSelectedCommodityType, influenceLevel, direction);
    this.newsService.addNewsItem(news);
  }
}
