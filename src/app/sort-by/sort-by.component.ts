import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SortArrowLookUp, SortArrowReset } from '../model/sort.model';
import { InvestmentService } from '../investment.service';

@Component({
  selector: 'seg-sort-by',
  templateUrl: './sort-by.component.html',
  styleUrls: ['./sort-by.component.css']
})
export class SortByComponent implements OnInit {
  @Input() arrowDirection: string;
  @Input() label: string;
  @Input() sortByValue: string;
  @Output() sort: EventEmitter<string>;

  order: string;
  sortOrderArrow: string;

  constructor(
    private investmentService: InvestmentService
  ) {
    this.order = 'neutral';
    this.sort = new EventEmitter<string>();
  }

  ngOnInit() {
    if (this.label === undefined) {
      this.label = this.sortByValue;
    }

    this.investmentService.getSortArrowReset().subscribe(
      (sortArrowReset: SortArrowReset) => {
        if (this.arrowDirection === sortArrowReset.arrowDirection
          && this.sortByValue !== sortArrowReset.skipSortByValue) {
            this.resetSortOrderArrow();
          }
      });

    this.updateSortOrderArrow();
  }

  updateSortOrderArrow(): void {
    this.sortOrderArrow = SortArrowLookUp[this.arrowDirection][this.order];
  }

  toggleSort(event: Event): void {
    event.stopPropagation();

    switch (this.order) {
      case 'forward':
        this.order = 'backward';
        break;

      case 'neutral':
      case 'backward':
        this.order = 'forward';
        break;
    }

    this.sort.emit(this.order);
    this.updateSortOrderArrow();
  }

  resetSortOrderArrow() {
    this.order = 'neutral';
    this.updateSortOrderArrow();
  }
}
