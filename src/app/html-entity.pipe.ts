import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'htmlEntity'
})
export class HtmlEntityPipe implements PipeTransform {

  constructor(
    private domSanitizer: DomSanitizer
  ) {}

  transform(htmlEntity: string): any {
    return this.domSanitizer.bypassSecurityTrustHtml(htmlEntity);
  }

}
