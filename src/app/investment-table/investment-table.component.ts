import { Component, OnInit, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { Country } from '../model/country.model';
import { InvestmentService } from '../investment.service';
import { SortByComponent } from '../sort-by/sort-by.component';
import { CountryRowComponent } from '../country-row/country-row.component';
import { CommodityRowComponent } from '../commodity-row/commodity-row.component';

@Component({
  selector: 'seg-investment-table',
  templateUrl: './investment-table.component.html',
  styleUrls: ['./investment-table.component.css']
})
export class InvestmentTableComponent implements OnInit {

  @ViewChild(CommodityRowComponent, { static: false})
  commodityRowComponent: CommodityRowComponent;

  @ViewChildren(CountryRowComponent)
  countryRowComponentQueryList: QueryList<CountryRowComponent>;

  countryList: Country[];

  constructor(
    private investmentService: InvestmentService
  ) { }

  ngOnInit() {
    this.investmentService.getCountryList().subscribe(
      (countryList: Country[]) => {
        this.countryList = countryList;
      });
  }
}
